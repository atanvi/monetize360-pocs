/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.monetize360.security.jwt.tokenutil;

import com.nimbusds.jwt.JWTClaimsSet;

/**
 *
 * @author Pradeepkm
 * @param <T> User Object
 */
public interface JwtClaimsSetCreator<T> {
    public JWTClaimsSet createsClaimSet(T user);
}

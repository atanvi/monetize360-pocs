/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.monetize360.security.jwt;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;
import org.springframework.security.core.userdetails.User;
/**
 *
 * @author pradeepkm
 */
@Component
public class JwtAuthenticationProvider implements AuthenticationProvider{
    public static final Logger LOGGER = LoggerFactory.getLogger(JwtAuthenticationProvider.class);
    
    private final JwtTokenParser tokenParser;
    
    @Autowired
    public JwtAuthenticationProvider(JwtTokenParser tokenParser) {
        this.tokenParser = tokenParser;
    }
    
    @Override
    public Authentication authenticate(Authentication a) {
        JwtAuthRequestToken jwtAuthReq = (JwtAuthRequestToken)a;
        String credentials = (String)jwtAuthReq.getCredentials();
        Object principal = jwtAuthReq.getPrincipal();
        LOGGER.debug("Credentials are token with length {} for given principal {}", credentials.length(), principal);
        String token = jwtAuthReq.getJwtToken();
        User user = tokenParser.parseToken(token);
        return new JwtAuthenticationToken(user, token, user.getAuthorities());
    }

    @Override
    public boolean supports(Class<?> type) {
        boolean retVal = JwtAuthRequestToken.class.isAssignableFrom(type);
        LOGGER.info("in supports with type as {}, retVal= {}", type.getName(), retVal);
        return retVal;
    }
}

/*
 * To change this license authHeader, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.monetize360.security.jwt;

import java.io.IOException;
import java.text.MessageFormat;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.AbstractAuthenticationProcessingFilter;

/**
 *
 * @author pradeepkm
 */
public class JwtAuthProcessingFilter extends AbstractAuthenticationProcessingFilter {
    /* 
     * Since super class has a logger which can be used by subclasses, we will use
     * the super classess logger ( courtesy sonar).
     */
    /**
     * Constructor for JWT processing filter. 
     * @param defaultFilterProcessingUrl the default Url for requiresAuthentication
     */
    public JwtAuthProcessingFilter(String defaultFilterProcessingUrl ) {
        //NOTE: Earlier we were doing super("/api/**"), 
        //changed the responsibility to app
        super(defaultFilterProcessingUrl);
    }

    @Override
    public boolean requiresAuthentication(HttpServletRequest request, HttpServletResponse response) {

        boolean retVal = super.requiresAuthentication(request, response);
        logger.info(MessageFormat.format("\n\n\n JWT NOTE: returning requiresAuthentication for URL {0} as {1}\n\n\n",
                request.getRequestURL(), retVal));
        return retVal;
    }

    @Override
    public Authentication attemptAuthentication(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {
        String authHeader = req.getHeader("Authorization");
        
        if (authHeader == null || !authHeader.startsWith("Bearer")) {
            logger.error("Missing token in header " + authHeader);
            throw new JwtAuthenticationException("Missing token in header");
        }
        
        String tokenStr = null;
        if (authHeader.length() > 7) { //"Bearer "
            tokenStr = authHeader.substring(7).trim();
        }
        
        if (tokenStr == null || tokenStr.isEmpty()) {
            logger.error("Malformed token " + tokenStr);
            throw new JwtAuthenticationException("Malformed token");
        }
        JwtAuthRequestToken authRequest = new JwtAuthRequestToken(tokenStr);
        return getAuthenticationManager().authenticate(authRequest);
    }

    @Override
    public void successfulAuthentication(HttpServletRequest req, HttpServletResponse resp, FilterChain chain, Authentication authResult) throws IOException, ServletException {
        logger.debug(MessageFormat.format("At successfulAuthentication {0} with auth class as {1}", authResult, authResult.getClass().getName()));
        super.successfulAuthentication(req, resp, chain, authResult);
        logger.debug("Post super.successfulAuthentication");
        chain.doFilter(req, resp);
    }
}

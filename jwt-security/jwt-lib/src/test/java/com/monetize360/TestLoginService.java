/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.monetize360;

import com.monetize360.security.jwt.JwtAuthenticationException;
import com.monetize360.security.jwt.tokenutil.JwtTokenCreator;

import java.util.HashMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

/**
 *
 * @author Pradeepkm
 */
@Service
public class TestLoginService {
    public static final Logger LOGGER = LoggerFactory.getLogger(TestLoginService.class);
    
    private final static HashMap<String, UserProfile> userMap = new HashMap<>();
    static {
        userMap.put("testuser@example.com", new UserProfile("testuser@example.com", "Test User", "abcd", "ROLE_USER"));
        userMap.put("testadmin@example.com", new UserProfile("testadmin@example.com", "Test Admin", "1234", "ROLE_ADMIN"));
    }
    @Autowired
    JwtTokenCreator<UserProfile> jwtTokenCreator;
    
    private boolean matchPasswd(UserProfile uProfile, String passwd) {
        return uProfile.getPasswd().equals(passwd);
    }
    
    public String login(String userEmail, String passwd) {
        Assert.hasText(userEmail, "Login ID cant be NULL");
        LOGGER.debug("Logging in with {}", userEmail);
        UserProfile uprofile = userMap.get(userEmail);
        if ( uprofile != null && matchPasswd(uprofile, passwd)) {
            return jwtTokenCreator.createSignedJwtToken(uprofile);
        }
        LOGGER.warn("Attempt to login with Invalid User ID {} OR password", userEmail);
        throw new JwtAuthenticationException("Invalid user name or Passwd"); 
    }
}

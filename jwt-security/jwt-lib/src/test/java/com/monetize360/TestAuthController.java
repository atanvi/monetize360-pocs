/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.monetize360;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.AuthenticationException;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.HttpStatusCodeException;

/**
 *
 * @author pradeepkm
 */
@RestController
@RequestMapping("/auth")
public class TestAuthController {

    public static final Logger LOGGER = LoggerFactory.getLogger(TestAuthController.class);

    @Autowired
    TestLoginService loginSvc;

    @SuppressWarnings("serial")
	@RequestMapping(value = "/login", method = RequestMethod.POST)
    public String login(String userId, String passwd) throws HttpStatusCodeException{
        LOGGER.info("logging in with userId {} ", userId);
        try {
            return loginSvc.login(userId, passwd);
        } catch (AuthenticationException ae) {
            throw new HttpStatusCodeException(HttpStatus.UNAUTHORIZED, ae.getMessage()) {
		
            };
        }
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.monetize360;

import java.security.Principal;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author pradeepkm
 */

@RestController
@RequestMapping("/api")
public class TestApiController {
    @RequestMapping(value="/resource", method = RequestMethod.GET)
    public Map<String,Object> home() {
        Map<String, Object> model = new HashMap<>();
        model.put("id", UUID.randomUUID().toString() );
        model.put("content", "HelloWorld");
        return model;
    }

    @RequestMapping(value="/user", method = RequestMethod.GET)
    public Principal user(Principal user) {
        System.out.println( "### User's class is ####" + user.getClass().getName());
        System.out.println("### User is ###" + user);
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        Object token = auth.getCredentials();
        Object principal = auth.getPrincipal();
        System.out.println("Token " + token);
        System.out.println("Principal " + principal);
        return user;
    }
}
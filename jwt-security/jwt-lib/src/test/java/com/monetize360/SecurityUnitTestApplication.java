package com.monetize360;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SecurityUnitTestApplication {

    public static void main(String[] args) {
        SpringApplication.run(SecurityUnitTestApplication.class, args);
    }

}

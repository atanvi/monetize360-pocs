/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.monetize360.security.jwt;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.verify;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import com.monetize360.TestLoginService;

/**
 *
 * @author Pradeepkm
 */
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class ApiAndFilterTests {

    @Autowired
    private TestRestTemplate restTemplateClient;

    @Autowired 
    JwtAuthenticationEntryPoint authEntryPoint;
    
    @Autowired
    TestLoginService loginSvc;
    
    @Test
    public void testLoginSuccess() {

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
        MultiValueMap<String, String> map = new LinkedMultiValueMap<>();
        map.add("userId", "testuser@example.com");
        map.add("passwd", "abcd");

        HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<>(map, headers);
        ResponseEntity<String> response = this.restTemplateClient.postForEntity("/auth/login/", request, String.class);
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
    }

    private HttpHeaders getAuthHeaders(String userName, String passwd) {
        HttpHeaders headers = new HttpHeaders();
        String jwt = loginSvc.login(userName, passwd);
        headers.add("Authorization", "Bearer " + jwt);
        return headers;
    }
    
    @Test
    public void testSuccessfulApi() {
        HttpHeaders authHeaders = getAuthHeaders("testuser@example.com",
                "abcd");
        ResponseEntity<String> response = this.restTemplateClient.exchange("/api/resource", HttpMethod.GET,
                new HttpEntity<>(authHeaders), String.class);
        System.out.println("Response is :" + response.getBody());
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(response.getBody()).containsPattern("\"id\":.*\"content\":\"HelloWorld\"");
    }
    
    @Test
    public void testGetPricipal() {
        HttpHeaders authHeaders = getAuthHeaders("testuser@example.com",
                "abcd");
        ResponseEntity<String> response = this.restTemplateClient.exchange("/api/user", HttpMethod.GET,
                new HttpEntity<>(authHeaders), String.class);
        System.out.println("Response is :" + response.getBody());
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        
    }
    
    @Test
    public void testAuthorizationFailureWithoutCredentials() {
        
        ResponseEntity<String> response = this.restTemplateClient.exchange("/api/resource", HttpMethod.GET, new HttpEntity<>(new HttpHeaders()), String.class);
        System.out.println("Response status code for WithoutCredentials " + response.getStatusCode());
        System.out.println("Response is :" + response.getBody());
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.UNAUTHORIZED);
    }
    
    @Test
    public void testAuthorizationFailureWithWrongAuthHeader() {
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", "Something else" );
        ResponseEntity<String> response = this.restTemplateClient.exchange("/api/resource", HttpMethod.GET, new HttpEntity<>(headers), String.class);
        System.out.println("Response status code for WrongAuthHeader " + response.getStatusCode());
        System.out.println("Response is :" + response.getBody());
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.UNAUTHORIZED);
    }
    
    @Test
    public void testAuthorizationFailureWithEmptyHeader() {
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", null );
        ResponseEntity<String> response = this.restTemplateClient.exchange("/api/resource", HttpMethod.GET, new HttpEntity<>(headers), String.class);
        System.out.println("Response status code for WrongAuthHeader " + response.getStatusCode());
        System.out.println("Response is :" + response.getBody());
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.UNAUTHORIZED);
    }
    
    @Test
    public void testAuthorizationFailureWithWrongToken() {
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", "Bearer " + "A Bad Token");
        ResponseEntity<String> response = this.restTemplateClient.exchange("/api/resource", HttpMethod.GET, new HttpEntity<>(headers), String.class);
        System.out.println("Response status code for WrongToken " + response.getStatusCode());
        System.out.println("Response is :" + response.getBody());
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.UNAUTHORIZED);
    }
    
    @Test
    public void testAuthorizationFailureWithEmptyToken() {
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", "Bearer");
        ResponseEntity<String> response = this.restTemplateClient.exchange("/api/resource", HttpMethod.GET, new HttpEntity<>(headers), String.class);
        System.out.println("Response status code for EmptyToken " + response.getStatusCode());
        System.out.println("Response is :" + response.getBody());
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.UNAUTHORIZED);
    }

    @Test
    public void testAuthenticationEntryPoint() throws IOException, ServletException {
        HttpServletResponse mockResponse = Mockito.mock(HttpServletResponse.class);
        HttpServletRequest mockRequest = Mockito.mock(HttpServletRequest.class);
        doNothing().when(mockResponse).sendError(HttpServletResponse.SC_UNAUTHORIZED, "JWT Unauthorized");
        authEntryPoint.commence(mockRequest, mockResponse, null);
        verify(mockResponse).sendError(HttpServletResponse.SC_UNAUTHORIZED, "JWT Unauthorized");
    }
    
}

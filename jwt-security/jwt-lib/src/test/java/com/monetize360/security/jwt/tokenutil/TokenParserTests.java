/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.monetize360.security.jwt.tokenutil;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.file.Paths;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.core.io.ClassPathResource;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;

import com.monetize360.security.jwt.JwtAuthenticationException;
import com.monetize360.security.jwt.JwtTokenParser;
import com.nimbusds.jose.JWSAlgorithm;
import com.nimbusds.jose.proc.SecurityContext;
import com.nimbusds.jwt.JWTClaimsSet;
import com.nimbusds.jwt.JWTClaimsSetTransformer;
import com.nimbusds.jwt.proc.JWTClaimsSetVerifier;

/**
 *
 * @author Pradeepkm
 */
public class TokenParserTests {

	private static final String rsaKeySet = "{\n" + "  \"keys\": [\n" + "    {\n" + "      \"kty\": \"RSA\",\n"
			+ "      \"d\": \"MA-5szmlYH6S9GlNHbnwEZUnq8HxRmXV91d4-LweMqNkcu_qWuGIufzdeCy7FJ2c4JiTfb7q1NK5i5_WjOgVn6r1bHOSkPjJZCX1vU0ICyHkys-WS5A_J8DEkIWLbgQlhGkPNCuVXMA8qSDZteK7DsgVEbjARO7w-h-dQJGyvow6xkF4Sj0dlTY1necQGuov2TrjGB9Un73BNhEAqdyUA51JJhoiHARZvGSGEdpXVlZVdbVALA2_CeIGTc1siL2FyIUDG2XQqg3V_45OfC8oGQyK0m1Ul-JquKr9l5ql4Ao_qNQpDeVYDmsAIBPAUWOrGGOBXuH-h90E_lg4TAMhQQ\",\n"
			+ "      \"e\": \"AQAB\",\n" + "      \"use\": \"sig\",\n" + "      \"kid\": \"test-app-key\",\n"
			+ "      \"alg\": \"RS256\",\n"
			+ "      \"n\": \"iPjC5NRpjwc1KCsfHYM0Luc2BkCI4y31Xt0jDAj7qrL_lKS6nBMZSZSWvm68jYW9GMZKb0LvKjs0v8JlOUtT2KR1o-yS_3Vciuy9YCuFMIOg2Sys5gXofQm2AaBlCFKCCERCaDrikdoTm7EgF3jRqWFtyQa_AXXvcmcfks1RW641XlEinOhawcfN5dnsU1qKo7jVrCIgGskqDWym_2sN4S2MriBvCyyY6hPqDWYngGXryrpawNs7hv1R2MMfYd8DW9A-n7ysvZ9RW6F9NwhTGyd_ReiLO4OkzU18zO0HW7MHVNU4NMNeQpznuqU7LrbYNzKHhkcTtTzq23vlsJSWzw\"\n"
			+ "    }\n" + "  ]\n" + "}";

	private static final String rsaKey = "{\n" + "  \"kty\": \"RSA\",\n"
			+ "  \"d\": \"MA-5szmlYH6S9GlNHbnwEZUnq8HxRmXV91d4-LweMqNkcu_qWuGIufzdeCy7FJ2c4JiTfb7q1NK5i5_WjOgVn6r1bHOSkPjJZCX1vU0ICyHkys-WS5A_J8DEkIWLbgQlhGkPNCuVXMA8qSDZteK7DsgVEbjARO7w-h-dQJGyvow6xkF4Sj0dlTY1necQGuov2TrjGB9Un73BNhEAqdyUA51JJhoiHARZvGSGEdpXVlZVdbVALA2_CeIGTc1siL2FyIUDG2XQqg3V_45OfC8oGQyK0m1Ul-JquKr9l5ql4Ao_qNQpDeVYDmsAIBPAUWOrGGOBXuH-h90E_lg4TAMhQQ\",\n"
			+ "  \"e\": \"AQAB\",\n" + "  \"use\": \"sig\",\n" + "  \"kid\": \"test-app-key\",\n"
			+ "  \"alg\": \"RS256\",\n"
			+ "  \"n\": \"iPjC5NRpjwc1KCsfHYM0Luc2BkCI4y31Xt0jDAj7qrL_lKS6nBMZSZSWvm68jYW9GMZKb0LvKjs0v8JlOUtT2KR1o-yS_3Vciuy9YCuFMIOg2Sys5gXofQm2AaBlCFKCCERCaDrikdoTm7EgF3jRqWFtyQa_AXXvcmcfks1RW641XlEinOhawcfN5dnsU1qKo7jVrCIgGskqDWym_2sN4S2MriBvCyyY6hPqDWYngGXryrpawNs7hv1R2MMfYd8DW9A-n7ysvZ9RW6F9NwhTGyd_ReiLO4OkzU18zO0HW7MHVNU4NMNeQpznuqU7LrbYNzKHhkcTtTzq23vlsJSWzw\"\n"
			+ "}";

	public static String badRsaKey = "{\n" + "  \"kty\": \"RSA\",\n" + "  \"d\": \"Some Garbage\",\n"
			+ "  \"e\": \"abcd\",\n" + "  \"use\": \"sig\",\n" + "  \"kid\": \"test-app-key\",\n"
			+ "  \"alg\": \"RS256\",\n" + "}";

	public static String badRsaKeySet = "{\n" + "  \"keys\": [\n" + "    {\n" + "      \"kty\": \"RSA\",\n"
			+ "      \"e\": \"AQAB\",\n" + "      \"use\": \"sig\",\n" + "      \"kid\": \"test-app-key\",\n"
			+ "      \"alg\": \"RS256\",\n" + "    }\n" + "  ]\n" + "}";

	private final JWTClaimsSetTransformer<User> claimSetTransformer = (JWTClaimsSet jcs) -> {
		try {
			String role = jcs.getStringClaim("role");
			List<GrantedAuthority> roles = new ArrayList<>();
			roles.add(new SimpleGrantedAuthority(role));
			return new User(jcs.getSubject(), "xxx", roles);
		} catch (ParseException ex) {
			ex.printStackTrace();
			throw new IllegalArgumentException("Bad Token");
		}
	};

	private final JWTClaimsSetVerifier claimsSetVerifier = (JWTClaimsSetVerifier) (JWTClaimsSet jwtcs,
			SecurityContext c) -> {
		// do nothing...
	};

	private static final String testRSA256Token = "eyJhbGciOiJSUzI1NiJ9.eyJzdWIiOiJqb2UiLCJyb2xlIjoiUk9MRV9VU0VSIiwiZXhwIjoxNTE2ODIzODc5LCJ1c2VySWQiOiIxMjM0In0.d1Z2p961d53lriBb0__JwA7WkVwGRvT48kscd3SZh4teLHqZ3AkX2k8pztsA76tXEElqdlyYbueLLf_bPSD7PIIoK8jzJDUwkIe7AtgVcSDvtU0ZamTc4jzBX1lfvH00sbBJwVj2HWuy4rHqoc6CBvcpH8W-Scfq3jWXaGrL9XmzNGCLQ51FjVY_wEvEowcYUhBE6b5KgR-lD5cKx3FBsPfvC1qgch50rIw2ASkZJVUAzsCii4LSA38cAsU5V7KBiKGPxBeNUHI78yQzyt7G0ukGM9uLT6wFVDxRlSpfdyVU1MCgkXb457PTPAaWf34uiiJweFySYd00QtVrMml2Ug";

	private static final String testHS384Token = "eyJhbGciOiJIUzM4NCJ9.eyJzdWIiOiJqb2UiLCJyb2xlIjoiUk9MRV9VU0VSIiwiZXhwIjoxNTIxMTExNTQyLCJ1c2VySWQiOiIxMjM0In0.vnGt6C35mW0aY6XsuHBu1kdLBXRmPoxCFyQFg8F7zhmFNnBRaPe6_oWR6KOf86kl";

	@SuppressWarnings("unchecked")
	@Test
	public void testRSA256jwtTokenParserWithJWK() {
		JwtTokenParser tokenParser = JwkBasedJwtTokenParser.newJwtTokenParser().withJwsAlgorithm(JWSAlgorithm.RS256)
				.withStringJWK(rsaKey).withJwtTransformer(claimSetTransformer).withJwtClaimsVerifier(claimsSetVerifier)
				.build();
		User u = tokenParser.parseToken(testRSA256Token);
		System.out.println("User is " + u);
		assertThat(u).isNotNull();
		assertThat(u.getUsername()).isEqualTo("joe");
		assertThat(u.getAuthorities()).isNotEmpty();
		assertThat(u.getAuthorities().iterator().next().getAuthority()).isEqualTo("ROLE_USER");
	}

	@SuppressWarnings("unchecked")
	@Test
	public void testRSA256jwtTokenParserWithJWKSet() throws IOException {
		JwtTokenParser tokenParser = JwkBasedJwtTokenParser.newJwtTokenParser().withJwsAlgorithm(JWSAlgorithm.RS256)
				.withStringJWKSetSource(rsaKeySet).withJwtTransformer(claimSetTransformer)
				.withJwtClaimsVerifier(claimsSetVerifier).build();
		User u = tokenParser.parseToken(testRSA256Token);
		Collection<GrantedAuthority> role = u.getAuthorities();
		System.out.println("User is " + u);
		assertThat(u).isNotNull();
		assertThat(u.getUsername()).isEqualTo("joe");
		assertThat(u.getAuthorities()).isNotEmpty();
		assertThat(u.getAuthorities().iterator().next().getAuthority()).isEqualTo("ROLE_USER");
	}

	@Test
	public void testHS384jwtTokenParserWithJWKSet() throws IOException {
		JwtTokenParser tokenParser = JwkBasedJwtTokenParser.newHS384FileJwkTokenParser(
				new ClassPathResource("jwkset-hs384.json").getFile(), claimSetTransformer, claimsSetVerifier);
		User u = tokenParser.parseToken(testHS384Token);
		Collection<GrantedAuthority> role = u.getAuthorities();
		System.out.println("User is " + u);
		assertThat(u).isNotNull();
		assertThat(u.getUsername()).isEqualTo("joe");
		assertThat(u.getAuthorities()).isNotEmpty();
		assertThat(u.getAuthorities().iterator().next().getAuthority()).isEqualTo("ROLE_USER");
	}

	@Test
	public void testRS256jwtTokenParserWithJWKSetFile() throws IOException {

		assertThrows(JwtAuthenticationException.class, () -> {
			JwtTokenParser tokenParser = JwkBasedJwtTokenParser.newRSA256FileJwkTokenParser(
					new ClassPathResource("jwkset-rsa256.json").getFile(), claimSetTransformer);
			System.out.println("\n\n\n Key parsed...\n\n\n");
			tokenParser.parseToken(testRSA256Token); // Token created with different signature, hence will get an
														// exception
		});

	}

	@SuppressWarnings("unchecked")
	@Test
	public void testErrorJWSKeySet() throws IOException {
		assertThrows(IllegalArgumentException.class, () -> {
			JwkBasedJwtTokenParser.newJwtTokenParser().withJwsAlgorithm(JWSAlgorithm.RS256)
					.withStringJWKSetSource(badRsaKeySet).withJwtTransformer(claimSetTransformer)
					.withJwtClaimsVerifier(claimsSetVerifier).build();
		});
	}

	@SuppressWarnings("unchecked")
	@Test
	public void testErrorJWSKeySetFile() throws IOException {
		assertThrows(IllegalArgumentException.class, () -> {
		JwkBasedJwtTokenParser.newJwtTokenParser().withJwsAlgorithm(JWSAlgorithm.RS256)
				.withFileJWKSetSource(new ClassPathResource("jwk-error.json").getFile())
				.withJwtTransformer(claimSetTransformer).withJwtClaimsVerifier(claimsSetVerifier).build();
		});
	}

	@SuppressWarnings("unchecked")
	@Test
	public void testErrorJWSKey() throws IOException {
		assertThrows(IllegalArgumentException.class, () -> {
		JwkBasedJwtTokenParser.newJwtTokenParser().withConfigurableJwtProcessor(null)
				.withJwsAlgorithm(JWSAlgorithm.RS256).withStringJWK(badRsaKey).withJwtTransformer(claimSetTransformer)
				.withJwtClaimsVerifier(claimsSetVerifier).build();
		});
	}

	@SuppressWarnings("unchecked")
	@Test
	public void testErrorJWSKeySetFileWithPath() throws IOException {
		assertThrows(IllegalArgumentException.class, () -> {
		JwkBasedJwtTokenParser.newJwtTokenParser().withJwsAlgorithm(JWSAlgorithm.RS256)
				.withFileJWKSetSource(Paths.get(new ClassPathResource("jwk-error.json").getPath()))
				.withJwtTransformer(claimSetTransformer).withJwtClaimsVerifier(claimsSetVerifier).build();
		});
	}

	@Test
	public void testJWSKeySetUrl() throws MalformedURLException {
		JwkBasedJwtTokenParser.newRSA256RemoteJwkTokenParser(new URL("http://connect2id.com/mykey"),
				claimSetTransformer, claimsSetVerifier);
	}

}

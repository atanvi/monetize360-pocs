/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.monetize360.security.jwt.tokenutil;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.io.IOException;
import java.nio.file.Paths;
import java.text.ParseException;
import java.util.Date;

import org.junit.jupiter.api.Test;
import org.springframework.core.io.ClassPathResource;

import com.monetize360.UserProfile;
import com.monetize360.security.jwt.JwtAuthenticationException;
import com.nimbusds.jose.JOSEException;
import com.nimbusds.jose.JWSAlgorithm;
import com.nimbusds.jose.jwk.JWKSet;
import com.nimbusds.jose.jwk.source.ImmutableJWKSet;
import com.nimbusds.jose.jwk.source.JWKSource;
import com.nimbusds.jose.proc.BadJOSEException;
import com.nimbusds.jose.proc.JWSKeySelector;
import com.nimbusds.jose.proc.JWSVerificationKeySelector;
import com.nimbusds.jwt.JWTClaimsSet;
import com.nimbusds.jwt.proc.ConfigurableJWTProcessor;
import com.nimbusds.jwt.proc.DefaultJWTProcessor;

/**
 *
 * @author Pradeepkm
 */
//@RunWith(SpringRunner.class)
//@SpringBootTest
public class TokenCreatorTests {

	public static String rsaKeySet = "{\n" + "  \"keys\": [\n" + "    {\n" + "      \"kty\": \"RSA\",\n"
			+ "      \"d\": \"MA-5szmlYH6S9GlNHbnwEZUnq8HxRmXV91d4-LweMqNkcu_qWuGIufzdeCy7FJ2c4JiTfb7q1NK5i5_WjOgVn6r1bHOSkPjJZCX1vU0ICyHkys-WS5A_J8DEkIWLbgQlhGkPNCuVXMA8qSDZteK7DsgVEbjARO7w-h-dQJGyvow6xkF4Sj0dlTY1necQGuov2TrjGB9Un73BNhEAqdyUA51JJhoiHARZvGSGEdpXVlZVdbVALA2_CeIGTc1siL2FyIUDG2XQqg3V_45OfC8oGQyK0m1Ul-JquKr9l5ql4Ao_qNQpDeVYDmsAIBPAUWOrGGOBXuH-h90E_lg4TAMhQQ\",\n"
			+ "      \"e\": \"AQAB\",\n" + "      \"use\": \"sig\",\n" + "      \"kid\": \"test-app-key\",\n"
			+ "      \"alg\": \"RS256\",\n"
			+ "      \"n\": \"iPjC5NRpjwc1KCsfHYM0Luc2BkCI4y31Xt0jDAj7qrL_lKS6nBMZSZSWvm68jYW9GMZKb0LvKjs0v8JlOUtT2KR1o-yS_3Vciuy9YCuFMIOg2Sys5gXofQm2AaBlCFKCCERCaDrikdoTm7EgF3jRqWFtyQa_AXXvcmcfks1RW641XlEinOhawcfN5dnsU1qKo7jVrCIgGskqDWym_2sN4S2MriBvCyyY6hPqDWYngGXryrpawNs7hv1R2MMfYd8DW9A-n7ysvZ9RW6F9NwhTGyd_ReiLO4OkzU18zO0HW7MHVNU4NMNeQpznuqU7LrbYNzKHhkcTtTzq23vlsJSWzw\"\n"
			+ "    }\n" + "  ]\n" + "}";

	public static String rsaKey = "{\n" + "  \"kty\": \"RSA\",\n"
			+ "  \"d\": \"MA-5szmlYH6S9GlNHbnwEZUnq8HxRmXV91d4-LweMqNkcu_qWuGIufzdeCy7FJ2c4JiTfb7q1NK5i5_WjOgVn6r1bHOSkPjJZCX1vU0ICyHkys-WS5A_J8DEkIWLbgQlhGkPNCuVXMA8qSDZteK7DsgVEbjARO7w-h-dQJGyvow6xkF4Sj0dlTY1necQGuov2TrjGB9Un73BNhEAqdyUA51JJhoiHARZvGSGEdpXVlZVdbVALA2_CeIGTc1siL2FyIUDG2XQqg3V_45OfC8oGQyK0m1Ul-JquKr9l5ql4Ao_qNQpDeVYDmsAIBPAUWOrGGOBXuH-h90E_lg4TAMhQQ\",\n"
			+ "  \"e\": \"AQAB\",\n" + "  \"use\": \"sig\",\n" + "  \"kid\": \"test-app-key\",\n"
			+ "  \"alg\": \"RS256\",\n"
			+ "  \"n\": \"iPjC5NRpjwc1KCsfHYM0Luc2BkCI4y31Xt0jDAj7qrL_lKS6nBMZSZSWvm68jYW9GMZKb0LvKjs0v8JlOUtT2KR1o-yS_3Vciuy9YCuFMIOg2Sys5gXofQm2AaBlCFKCCERCaDrikdoTm7EgF3jRqWFtyQa_AXXvcmcfks1RW641XlEinOhawcfN5dnsU1qKo7jVrCIgGskqDWym_2sN4S2MriBvCyyY6hPqDWYngGXryrpawNs7hv1R2MMfYd8DW9A-n7ysvZ9RW6F9NwhTGyd_ReiLO4OkzU18zO0HW7MHVNU4NMNeQpznuqU7LrbYNzKHhkcTtTzq23vlsJSWzw\"\n"
			+ "}";

	public static String badRsaKey = "{\n" + "  \"kty\": \"RSA\",\n" + "  \"d\": \"Some Garbage\",\n"
			+ "  \"e\": \"abcd\",\n" + "  \"use\": \"sig\",\n" + "  \"kid\": \"test-app-key\",\n"
			+ "  \"alg\": \"RS256\",\n" + "  \"n\": \"somethinghere\"\n" + "}";

	public static String badRsaKey2 = "{\n" + "  \"kty\": \"RSA\",\n" + "  \"d\": \"Some Garbage\",\n"
			+ "  \"e\": \"abcd\",\n" + "  \"use\": \"sig\",\n" + "  \"kid\": \"test-app-key\",\n"
			+ "  \"alg\": \"RS256\",\n" + "}";

	public static String badRsaKeySet = "{\n" + "  \"keys\": [\n" + "    {\n" + "      \"kty\": \"RSA\",\n"
			+ "      \"e\": \"AQAB\",\n" + "      \"use\": \"sig\",\n" + "      \"kid\": \"test-app-key\",\n"
			+ "      \"alg\": \"RS256\",\n" + "    }\n" + "  ]\n" + "}";

	private final JwtClaimsSetCreator<UserProfile> claimsCreator = (UserProfile user) -> {
		return new JWTClaimsSet.Builder().subject(user.getUserName())
				// .issuer("https://c2id.com")
				.expirationTime(new Date(new Date().getTime() + 60 * 60 * 1000)).claim("userId", user.getUserId())
				.claim("role", user.getRole()).build();
	};

	private JWTClaimsSet parseRSAToken(String token) throws ParseException, BadJOSEException, JOSEException {
		ConfigurableJWTProcessor jwtProcessor = new DefaultJWTProcessor();
		JWKSet keySet = JWKSet.parse(rsaKeySet);
		JWKSource keySource = new ImmutableJWKSet(keySet);
		JWSAlgorithm expectedJWSAlg = JWSAlgorithm.RS256;
		JWSKeySelector keySelector = new JWSVerificationKeySelector(expectedJWSAlg, keySource);
		jwtProcessor.setJWSKeySelector(keySelector);
		jwtProcessor.setJWSKeySelector(keySelector);
		return jwtProcessor.process(token, null);
	}

	@Test
	public void testJwtTokenCreationWithJWKString() throws ParseException, BadJOSEException, JOSEException {
		System.out.println("testJwtTokenCreationWithJWKString");
		JwtTokenCreator<UserProfile> tokenCreator = JwtTokenCreator.newJwtTokenCreator(UserProfile.class)
				.withJWKSetString(rsaKeySet, "test-app-key").withClaimsSetCreator(claimsCreator).build();
		UserProfile userProfile = new UserProfile("1234", "joe", "***", "ROLE_USER");
		String token = tokenCreator.createSignedJwtToken(userProfile);
		System.out.println("Generated Token " + token);
		assertThat(token).isNotEmpty();
		JWTClaimsSet claims = parseRSAToken(token);
		assertThat(claims.getSubject()).isEqualTo(userProfile.getUserName());
		assertThat(claims.getStringClaim("userId")).isEqualTo(userProfile.getUserId());
		assertThat(claims.getStringClaim("role")).isEqualTo(userProfile.getRole());
	}

	@Test
	public void testJwtTokenCreationWithJWKSetString()
			throws ParseException, BadJOSEException, JOSEException, IOException, IOException {
		System.out.println("testing testJwtTokenCreationWithJWKSetString");
		JwtTokenCreator<UserProfile> tokenCreator = JwtTokenCreator.newJwtTokenCreator(UserProfile.class)
				.withJWK(rsaKey).withClaimsSetCreator(claimsCreator).build();
		UserProfile userProfile = new UserProfile("1234", "joe", "***", "ROLE_USER");
		String token = tokenCreator.createSignedJwtToken(userProfile);
		System.out.println("Generated Token " + token);
		assertThat(token).isNotEmpty();
		JWTClaimsSet claims = parseRSAToken(token);
		assertThat(claims.getSubject()).isEqualTo(userProfile.getUserName());
		assertThat(claims.getStringClaim("userId")).isEqualTo(userProfile.getUserId());
		assertThat(claims.getStringClaim("role")).isEqualTo(userProfile.getRole());
	}

	@Test
	public void testJwtTokenCreationWithJWKSetFile()
			throws ParseException, BadJOSEException, JOSEException, IOException {
		System.out.println("testing testJwtTokenCreationWithJWKSetFile");

		JwtTokenCreator<UserProfile> tokenCreator = JwtTokenCreator.newJwtTokenCreator(UserProfile.class)
				.withJWKSetFile(new ClassPathResource("jwkset-rsa256.json").getFile(), "testkey-rsa256")
				.withClaimsSetCreator(claimsCreator).build();
		UserProfile userProfile = new UserProfile("1234", "joe", "***", "ROLE_USER");
		String token = tokenCreator.createSignedJwtToken(userProfile);
		System.out.println("Generated Token " + token);
		assertThat(token).isNotEmpty();
		// NOTE: We need not parse token here, since we are testing the creation cases
	}

	@Test
	public void testJwtTokenCreationWithJWKSetFilePath()
			throws ParseException, BadJOSEException, JOSEException, IOException {
		System.out.println("testing testJwtTokenCreationWithJWKSetFilePath");

		assertThrows(IllegalArgumentException.class, () -> {
			JwtTokenCreator<UserProfile> tokenCreator = JwtTokenCreator.newJwtTokenCreator(UserProfile.class)
					.withJWKSetFilePath(Paths.get(new ClassPathResource("jwk.json").getPath()), "testkey-rsa256") // cant
																													// get
																													// file
																													// from
																													// path
																													// from
																													// classpath
					.withClaimsSetCreator(claimsCreator).build();
			UserProfile userProfile = new UserProfile("1234", "joe", "***", "ROLE_USER");
			String token = tokenCreator.createSignedJwtToken(userProfile);
			System.out.println("Generated Token " + token);
			assertThat(token).isEmpty();
			// NOTE: We need not parse token here, since we are testing the creation cases
		});

	}

	@Test
	public void testJwtTokenCreationWithWithEC256JWKSetFile() throws IOException {
		System.out.println("testing testJwtTokenCreationWithWithEC256JWKSetFile");
		JwtTokenCreator<UserProfile> tokenCreator = JwtTokenCreator.newJwtTokenCreator(UserProfile.class)
				.withJWKSetFile(new ClassPathResource("jwkset-ec256.json").getFile(), "testkey-ec256")
				.withClaimsSetCreator(claimsCreator).build();
		UserProfile userProfile = new UserProfile("1234", "joe", "***", "ROLE_USER");
		String token = tokenCreator.createSignedJwtToken(userProfile);
		assertThat(token).isNotEmpty();
	}

	@Test
	public void testJwtTokenCreationWithWithHS384JWKSetFile() throws IOException {
		System.out.println("testing testJwtTokenCreationWithWithHS384JWKSetFile");
		JwtTokenCreator<UserProfile> tokenCreator = JwtTokenCreator.newJwtTokenCreator(UserProfile.class)
				.withJWKSetFile(new ClassPathResource("jwkset-hs384.json").getFile(), "testkey-hs384")
				.withClaimsSetCreator(claimsCreator).build();
		UserProfile userProfile = new UserProfile("1234", "joe", "***", "ROLE_USER");
		String token = tokenCreator.createSignedJwtToken(userProfile);
		System.out.println("HS384 token is: " + token);
		assertThat(token).isNotEmpty();
	}

	@Test
	public void testJwtTokenCreationWithErrorJWKSetFile() throws IOException {
		System.out.println("testing testJwtTokenCreationWithErrorJWKSetFile");
		assertThrows(IllegalArgumentException.class, () -> {
			JwtTokenCreator<UserProfile> tokenCreator = JwtTokenCreator.newJwtTokenCreator(UserProfile.class)
					.withJWKSetFile(new ClassPathResource("jwk-error.json").getFile(), "test-app-key")
					.withClaimsSetCreator(claimsCreator).build();
			UserProfile userProfile = new UserProfile("1234", "joe", "***", "ROLE_USER");
			String token = tokenCreator.createSignedJwtToken(userProfile);
			assertThat(token).isEmpty();
		});

	}

	@Test
	public void testJwtTokenCreationWithErrorJWKString() {
		System.out.println("testing testJwtTokenCreationWithErrorJWKString");
		assertThrows(JwtAuthenticationException.class, () -> {
			JwtTokenCreator<UserProfile> tokenCreator = JwtTokenCreator.newJwtTokenCreator(UserProfile.class)
					.withJWK(badRsaKey).withClaimsSetCreator(claimsCreator).build();
			UserProfile userProfile = new UserProfile("1234", "joe", "***", "ROLE_USER");
			String token = tokenCreator.createSignedJwtToken(userProfile);
			System.out.println("Token is " + token);
			assertThat(token).isEmpty();
		});

	}

	@Test
	public void testJwtTokenCreationWithErrorJWKString2() {
		System.out.println("testing testJwtTokenCreationWithErrorJWKString2");

		assertThrows(IllegalArgumentException.class, () -> {
			JwtTokenCreator<UserProfile> tokenCreator = JwtTokenCreator.newJwtTokenCreator(UserProfile.class)
					.withJWK(badRsaKey2).withClaimsSetCreator(claimsCreator).build();
			UserProfile userProfile = new UserProfile("1234", "joe", "***", "ROLE_USER");
			String token = tokenCreator.createSignedJwtToken(userProfile);
			System.out.println("Token is " + token);
			assertThat(token).isEmpty();
		});

	}

	@Test
	public void testJwtTokenCreationWithErrorJWKSetString() {
		System.out.println("testing testJwtTokenCreationWithErrorJWKSetString");
		assertThrows(IllegalArgumentException.class,()->{
			JwtTokenCreator<UserProfile> tokenCreator = JwtTokenCreator.newJwtTokenCreator(UserProfile.class)
					.withJWKSetString(badRsaKeySet, "test-app-key").withClaimsSetCreator(claimsCreator).build();
			UserProfile userProfile = new UserProfile("1234", "joe", "***", "ROLE_USER");
			String token = tokenCreator.createSignedJwtToken(userProfile);
			System.out.println("Token is " + token);
			assertThat(token).isEmpty();
		});
		
	}
}

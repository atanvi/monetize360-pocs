/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.monetize360.security.config;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.security.SecurityProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import com.monetize360.UserProfile;
import com.monetize360.security.jwt.JwtAuthProcessingFilter;
import com.monetize360.security.jwt.JwtAuthenticationEntryPoint;
import com.monetize360.security.jwt.JwtAuthenticationProvider;
import com.monetize360.security.jwt.JwtAuthenticationSuccessHandler;
import com.monetize360.security.jwt.JwtTokenParser;
import com.monetize360.security.jwt.tokenutil.JwkBasedJwtTokenParser;
import com.monetize360.security.jwt.tokenutil.JwtTokenCreator;
import com.nimbusds.jose.JWSAlgorithm;
import com.nimbusds.jwt.JWTClaimsSet;

/**
 *
 * @author pradeepkm
 */
@Configuration
@EnableWebSecurity
@Order(SecurityProperties.IGNORED_ORDER)
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    public static final Logger LOGGER = LoggerFactory.getLogger(SecurityConfig.class);

    private static final String rsaKeySet = "{\n"
            + "  \"keys\": [\n"
            + "    {\n"
            + "      \"kty\": \"RSA\",\n"
            + "      \"d\": \"MA-5szmlYH6S9GlNHbnwEZUnq8HxRmXV91d4-LweMqNkcu_qWuGIufzdeCy7FJ2c4JiTfb7q1NK5i5_WjOgVn6r1bHOSkPjJZCX1vU0ICyHkys-WS5A_J8DEkIWLbgQlhGkPNCuVXMA8qSDZteK7DsgVEbjARO7w-h-dQJGyvow6xkF4Sj0dlTY1necQGuov2TrjGB9Un73BNhEAqdyUA51JJhoiHARZvGSGEdpXVlZVdbVALA2_CeIGTc1siL2FyIUDG2XQqg3V_45OfC8oGQyK0m1Ul-JquKr9l5ql4Ao_qNQpDeVYDmsAIBPAUWOrGGOBXuH-h90E_lg4TAMhQQ\",\n"
            + "      \"e\": \"AQAB\",\n"
            + "      \"use\": \"sig\",\n"
            + "      \"kid\": \"test-app-key\",\n"
            + "      \"alg\": \"RS256\",\n"
            + "      \"n\": \"iPjC5NRpjwc1KCsfHYM0Luc2BkCI4y31Xt0jDAj7qrL_lKS6nBMZSZSWvm68jYW9GMZKb0LvKjs0v8JlOUtT2KR1o-yS_3Vciuy9YCuFMIOg2Sys5gXofQm2AaBlCFKCCERCaDrikdoTm7EgF3jRqWFtyQa_AXXvcmcfks1RW641XlEinOhawcfN5dnsU1qKo7jVrCIgGskqDWym_2sN4S2MriBvCyyY6hPqDWYngGXryrpawNs7hv1R2MMfYd8DW9A-n7ysvZ9RW6F9NwhTGyd_ReiLO4OkzU18zO0HW7MHVNU4NMNeQpznuqU7LrbYNzKHhkcTtTzq23vlsJSWzw\"\n"
            + "    }\n"
            + "  ]\n"
            + "}";
    
    @Autowired
    private JwtAuthenticationProvider jwtAuthenticationProvider;

    @Autowired
    private JwtAuthenticationEntryPoint jwtAuthenticationEntryPoint;

    @Bean
    JwtTokenCreator getJwtTokenCreator() {
        return JwtTokenCreator.newJwtTokenCreator(UserProfile.class)
                .withJWKSetString(rsaKeySet, "test-app-key")
                .withClaimsSetCreator((UserProfile user) -> {
                    return new JWTClaimsSet.Builder()
                            .subject(user.getUserId())
                            //                    .issuer("https://c2id.com")
                            .expirationTime(new Date(new Date().getTime() + 60 * 60 * 1000))
                            .claim("UserName", user.getUserName())
                            .claim("Role", user.getRole())
                            .build();
                })
                .build();
    }

    @Bean
    JwtTokenParser getJwtTokenParser() {
        return JwkBasedJwtTokenParser.newJwtTokenParser()
                .withStringJWKSetSource(rsaKeySet)
                .withJwsAlgorithm(JWSAlgorithm.RS256)
                .withJwtTransformer((JWTClaimsSet jwtcs) -> {
                    try {
                        String role = jwtcs.getStringClaim("Role");
                        String userName = jwtcs.getStringClaim("UserName");
                        List<GrantedAuthority> roles = new ArrayList<>();
                        roles.add(new SimpleGrantedAuthority(role));
                        return new TestAppUser(userName, jwtcs.getSubject(), role, roles);
                    } catch (ParseException ex) {
                        LOGGER.error("BAD token : Error Transforming Claims to Principal {}", ex.getMessage());
                        throw new IllegalArgumentException("Bad Token");
                    }
                })
                .build();
    }
    
    
    @Bean
    public JwtAuthProcessingFilter authenticationTokenFilterBean() throws Exception {
        JwtAuthProcessingFilter filter = new JwtAuthProcessingFilter("/api/**");
        filter.setAuthenticationManager(authenticationManagerBean());
        filter.setAuthenticationSuccessHandler(new JwtAuthenticationSuccessHandler());
        return filter;
    }

    @Bean
    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }

    @Override
    protected void configure(AuthenticationManagerBuilder authManagerBuilder) throws Exception {
        LOGGER.info("\n\n\n\nJWT NOTE: configuring authentication provider {}\n\n\n\n", this.jwtAuthenticationProvider);
        authManagerBuilder.authenticationProvider(this.jwtAuthenticationProvider);
    }

    @Override
    protected void configure(HttpSecurity httpSecurity) throws Exception {

        LOGGER.debug("\n\n\n\nJWT NOTE: authentication entry point {}, authentication provider {} \n\n\n\n",
                jwtAuthenticationEntryPoint, jwtAuthenticationProvider);

        httpSecurity.csrf().disable()
                .exceptionHandling().authenticationEntryPoint(jwtAuthenticationEntryPoint).and()
                //dont create sessions
                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS).and()
                .authorizeRequests()
                .antMatchers("/").permitAll()
                .antMatchers("/*.html").permitAll()
                .antMatchers("/favicon.ico").permitAll()
                .antMatchers("**/*.html").permitAll()
                .antMatchers("**/*.css").permitAll()
                .antMatchers("**/*.js").permitAll()
                //		NOTE: The following way of pattern matching is preferred,  however this needs to be tested  
                //                    .antMatchers(HttpMethod.GET,
                //                            "/",
                //                            "/*.html",
                //                            "/favicon.ico",
                //                            "/**/*.html",
                //                            "/**/*.css",
                //                            "/**/*.js").permitAll()
                .antMatchers("/auth/**").permitAll()
                .anyRequest().authenticated();

        httpSecurity.addFilterBefore(authenticationTokenFilterBean(),
                UsernamePasswordAuthenticationFilter.class);

        httpSecurity.headers().cacheControl();
    }
}

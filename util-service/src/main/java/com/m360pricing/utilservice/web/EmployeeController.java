package com.m360pricing.utilservice.web;
import com.m360pricing.utilservice.domain.M360PageRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;


@RestController
public class EmployeeController {

        @Autowired
        private EmployeeRepo employeeRepo;
        @GetMapping("/emplist")
        public Page<Employee> getEmployeesGet( @RequestParam(defaultValue = "0") int page,
                                               @RequestParam(defaultValue = "10") int size,
                                               @RequestParam(required = false) String[] sort){

                Pageable pageable = M360PageRequest.getPageable(page,size,sort);
                Page responsePage = employeeRepo.findAll(pageable);
                System.out.println(responsePage.getContent());
                return responsePage;
        }

        @PostMapping("/showlist")
        public Page<Employee> getEmployeesByPost(@RequestParam(defaultValue = "0") int page,
                                                 @RequestParam(defaultValue = "10") int size,
                                                 @RequestParam(required = false) String[] sort){
                Pageable pageable = M360PageRequest.getPageable(page,size,sort);
                return employeeRepo.findAll(pageable);
        }
}

package com.m360pricing.utilservice.domain;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Order;

import java.util.ArrayList;
import java.util.List;

@Slf4j
public class M360PageRequest {
      public static Pageable getPageable(int page, int size, String[] sort) {
        if (page < 0 || size < 0) {
            throw new IllegalArgumentException("Page number/size can't be negative number");
        }
        if (sort == null || sort.length == 0) {
            log.debug("Requested for Pagination only, Sorting fields are empty/null ");
            return PageRequest.of(page, size);
        } else {
            List<Order> orders = new ArrayList<Order>();
            for (String sortOrder : sort) {
                String[] arr = sortOrder.split(",");
                if (arr.length == 2) {
                    orders.add(new Order(Sort.Direction.fromString(arr[1]), arr[0]));
                    log.debug("Direction of the field {} is {}", arr[0], Sort.Direction.fromString(arr[1]));
                } else {
                    log.debug("Direction of the field {} is empty, so it picked default(ASC) ", arr[0]);
                    orders.add(new Order(Sort.Direction.ASC, arr[0]));
                }
            }
            return PageRequest.of(page, size, Sort.by(orders));
        }
    }
}

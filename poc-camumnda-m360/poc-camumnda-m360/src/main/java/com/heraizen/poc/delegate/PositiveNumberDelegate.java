package com.heraizen.poc.delegate;

import javax.inject.Named;

import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Named
public class PositiveNumberDelegate implements JavaDelegate {

	private static final Logger log = LoggerFactory.getLogger(PositiveNumberDelegate.class);

	@Override
	public void execute(DelegateExecution execution) throws Exception {
			String  strnum = (String) execution.getVariable("usernumber");
			double num = Double.parseDouble(strnum);
			log.info("User input value received as :{}",num);
			num = -num;
			num = num + 1;
			log.info("User input value set as :{}",num);
			execution.setVariable("usernumber", num);
	}
}

package com.heraizen.acp.routes;
import com.amazonaws.regions.Regions;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.component.aws.s3.S3Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class FileReaderRouter extends RouteBuilder {

    @Autowired
    private DataCount dataCount;

    @Override
    public void configure() throws Exception {
        String pathName =   System.getProperty("java.io.tmpdir")+"//userdata";
            from("file:"+pathName+"?recursive=true")
           .bean(dataCount)
           .convertBodyTo(byte[].class)

           .setHeader(S3Constants.CONTENT_LENGTH, simple("${in.header.CamelFileLength}"))
                    .setHeader(S3Constants.KEY,simple("demo/player/${in.header.CamelFileNameOnly}"))
                    .to("aws-s3://m360poc"
                            + "?deleteAfterWrite=false&region="+Regions.AP_SOUTH_1.name()
                            + "&accessKey=AKIAQ2PGYVQITNCVCQNH"
                            + "&secretKey=RAW(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)")
                    .log("Uploaded successfully");
        }
    
}

package com.heraizen.acp.routes;

import org.springframework.stereotype.Component;

import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
public class DataCount {
        public void lineCount(String data){
           String[] lines= data.split(System.getProperty("line.separator"));
           log.info("Total lines found :{}",lines.length);
        }
}

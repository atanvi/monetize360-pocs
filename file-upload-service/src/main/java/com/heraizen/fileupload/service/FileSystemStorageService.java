package com.heraizen.fileupload.service;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;



@Service
public class FileSystemStorageService implements StorageService {

    private  Path rootLocation;
    String pathName;
    {
        try{
            pathName =   System.getProperty("java.io.tmpdir");
            pathName = pathName+"//userdata";
            File file = new File(pathName);
            if(!file.exists())
                file.mkdir();
            System.out.println("Directory with name : "+pathName);
            this.rootLocation = Paths.get(pathName);
        }catch(Exception e){
            e.printStackTrace();;
        }
    }

    @Override
    public void store(MultipartFile file) {
        try {
			if (file.isEmpty()) {
				throw new StorageException("Failed to store empty file " + file.getOriginalFilename());
			}
			Files.copy(file.getInputStream(), this.rootLocation.resolve(file.getOriginalFilename()));
		} catch (IOException e) {
			throw new StorageException("Failed to store file " + file.getOriginalFilename(), e);
		}
    }
    
}

package com.monetize360.multitenancy.config;

import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Properties;

import javax.persistence.EntityManagerFactory;

import org.hibernate.MultiTenancyStrategy;
import org.hibernate.cfg.Environment;
import org.hibernate.context.spi.CurrentTenantIdentifierResolver;
import org.hibernate.engine.jdbc.connections.spi.MultiTenantConnectionProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.orm.jpa.JpaProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.core.io.ClassPathResource;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;

public abstract class M360DefaultMultiTenantConfiguration {

	@Autowired
	protected JpaProperties jpaProperties;

	@Value("${m360.tenant_details.file:m360-tenants.yml}")
	protected String tenantYamlFileName;

	@Value("${m60.default_tenant.name}")
	protected String defaultTenantId;

	@Value("${m360.domain_pkg.scan}")
	protected String[] basePackagesToScan;

	@Value("${m360.hibernate.props:m360-hibernate.properties}")
	protected String hibernateProperties;

	public abstract MultiTenantConnectionProvider multiTenantConnectionProvider();

	public abstract CurrentTenantIdentifierResolver currentTenantIdentifierResolver();

	@Bean
	public LocalContainerEntityManagerFactoryBean entityManagerFactoryBean(
			MultiTenantConnectionProvider multiTenantConnectionProvider,
			CurrentTenantIdentifierResolver currentTenantIdentifierResolver) throws IOException {

		Map<String, Object> hibernateProps = new LinkedHashMap<>();
		Properties properties = new Properties();
		properties.load(new ClassPathResource(hibernateProperties).getInputStream());
		hibernateProps.putAll(this.jpaProperties.getProperties());
		properties.entrySet().forEach((ele) -> {
			hibernateProps.put((String) ele.getKey(), ele.getValue());
		});
		hibernateProps.put(Environment.MULTI_TENANT, MultiTenancyStrategy.DATABASE);
		hibernateProps.put(Environment.MULTI_TENANT_CONNECTION_PROVIDER, multiTenantConnectionProvider);
		hibernateProps.put(Environment.MULTI_TENANT_IDENTIFIER_RESOLVER, currentTenantIdentifierResolver);

		LocalContainerEntityManagerFactoryBean result = new LocalContainerEntityManagerFactoryBean();
		result.setPackagesToScan(basePackagesToScan);
		result.setJpaVendorAdapter(new HibernateJpaVendorAdapter());
		result.setJpaPropertyMap(hibernateProps);
		return result;
	}

	@Bean
	public EntityManagerFactory entityManagerFactory(LocalContainerEntityManagerFactoryBean entityManagerFactoryBean) {
		return entityManagerFactoryBean.getObject();
	}

	@Bean
	public PlatformTransactionManager txManager(EntityManagerFactory entityManagerFactory) {
		JpaTransactionManager transactionManager = new JpaTransactionManager();
		transactionManager.setEntityManagerFactory(entityManagerFactory);
		return transactionManager;
	}
}

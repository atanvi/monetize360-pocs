package com.monetize360.multitenancy.exception;

public class MultiTenantDataSourceException extends RuntimeException {
	private static final long serialVersionUID = 1L;

	public MultiTenantDataSourceException(String message) {
		super(message);
	}

	public MultiTenantDataSourceException(String message, Throwable actualException) {
		super(message, actualException);
	}
}
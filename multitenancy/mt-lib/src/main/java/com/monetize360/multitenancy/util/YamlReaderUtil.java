package com.monetize360.multitenancy.util;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.Resource;
import org.yaml.snakeyaml.Yaml;
import org.yaml.snakeyaml.constructor.Constructor;

import com.monetize360.multitenancy.domain.TenantDetails;
import com.monetize360.multitenancy.exception.MultiTenantIOExcpetion;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class YamlReaderUtil {

	public static List<TenantDetails> loadTenantRepoDetailsFromCp(String yamlFile) {
		return loadTenantDetails(new ClassPathResource(yamlFile));
	}

	public static List<TenantDetails> loadTenantRepoDetailsFromPath(Path filePath) {
		return loadTenantDetails(new FileSystemResource(filePath.toString()));
	}

	public static List<TenantDetails> loadTenantDetails(Resource yamlResource) {
		log.info("Loading tenant details from {}", yamlResource);
		Yaml yaml = new Yaml(new Constructor(TenantDetails.class));
		List<TenantDetails> tenantDetailsList = new ArrayList<>();
		try (InputStream in = yamlResource.getInputStream()) {
			yaml.loadAll(in).forEach(o -> tenantDetailsList.add((TenantDetails) o));
		} catch (IOException ex) {
			String errMsg = String.format("Error reading YAML Tenant configuration file %s", ex.getMessage());
			log.error(errMsg);
			log.debug("Stack trace :\n", ex);
			throw new MultiTenantIOExcpetion(errMsg, ex);
		}
		log.info("Loaded {} tenant's repo details", tenantDetailsList.size());
		return tenantDetailsList;
	}

}

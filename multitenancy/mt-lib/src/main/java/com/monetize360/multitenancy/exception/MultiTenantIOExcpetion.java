package com.monetize360.multitenancy.exception;

public class MultiTenantIOExcpetion extends RuntimeException {
	private static final long serialVersionUID = 1L;

	public MultiTenantIOExcpetion(String message) {
		super(message);
	}

	public MultiTenantIOExcpetion(String message, Throwable actualException) {
		super(message, actualException);
	}
}
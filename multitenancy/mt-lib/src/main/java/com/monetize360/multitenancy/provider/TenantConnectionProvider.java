package com.monetize360.multitenancy.provider;

import java.nio.file.Path;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;
import javax.annotation.PreDestroy;
import javax.sql.DataSource;
import org.hibernate.engine.jdbc.connections.spi.AbstractDataSourceBasedMultiTenantConnectionProviderImpl;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.Resource;
import org.springframework.util.Assert;
import org.springframework.util.StringUtils;
import com.monetize360.multitenancy.domain.TenantDetails;
import com.monetize360.multitenancy.exception.MultiTenantDataSourceException;
import com.monetize360.multitenancy.util.YamlReaderUtil;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class TenantConnectionProvider extends AbstractDataSourceBasedMultiTenantConnectionProviderImpl {

	private static final long serialVersionUID = 1L;

	private final String defaultTenant;
	private final Map<String, TenantDetails> tenantDetailsMap = new ConcurrentHashMap<>();
	private final Map<String, DataSource> tenantDataSource = new ConcurrentHashMap<>();

	TenantConnectionProvider(List<TenantDetails> tenantDtailsList, String defaultTenant) {
		Assert.notNull(tenantDtailsList, "Tenant Details cant be NULL, while constructing  datasource");
		Assert.notEmpty(tenantDtailsList, "Tenant Details cant be NULL, while constructing  datasource");
		Assert.hasText(defaultTenant, "Default Tenant must be provided (cant be NULL)");
		tenantDtailsList.forEach(t -> tenantDetailsMap.put(t.getTenantId(), t));
		this.defaultTenant = defaultTenant;
	}

	@Override
	protected DataSource selectAnyDataSource() {
		return selectDataSource(defaultTenant);
	}

	@Override
	protected DataSource selectDataSource(String tenantId) {
		Optional<DataSource> dataSource = StringUtils.hasText(tenantId) ? findTenantDataSource(tenantId)
				: Optional.empty();
		return dataSource.orElseThrow(() -> {
			log.error("Datasource for Tenant {} Not Found Setup (Not Setup)", tenantId);
			return new MultiTenantDataSourceException(
					"Tenant Datasource is NOT setup...hence cant do any operations, contact administrator...");
		});
	}

	private DataSource mapToDataSource(TenantDetails tenantDetails) {
		DataSourceBuilder<?> dataBuilder = DataSourceBuilder.create().url(tenantDetails.getDbUrl())
				.username(tenantDetails.getDbUser()).password(tenantDetails.getDbPassword());
		return dataBuilder.build();
	}

	public Optional<DataSource> findTenantDataSource(String tenantId) {
		Assert.hasText(tenantId, "Tenant ID can not be NULL (while finding Tenant)");
		TenantDetails tenantDetails = tenantDetailsMap.get(tenantId);
		if (tenantDetails != null) {
			DataSource dataSource = tenantDataSource.computeIfAbsent(tenantId, ds -> {
				try {
					log.debug(" Trying to initiate datasource " + "for Tenant id {}", tenantId);
					return mapToDataSource(tenantDetails);
				} catch (Exception e) {
					String errMessage = String.format("Could not create datasource for Tenant %s at Tenant",
							tenantDetails.getTenantId());
					log.error(errMessage + " Cause : " + e.getMessage(), e);
					throw new MultiTenantDataSourceException(errMessage, e);
				}
			});
			return Optional.of(dataSource);
		}
		return Optional.empty();
	}

	@PreDestroy
	public void close() {
		tenantDataSource.forEach((k, ds) -> {
			try {
				if (ds != null)
					ds.getConnection().close();
			} catch (SQLException ex) {
				ex.printStackTrace();
				String errMsg = String.format("While closing DataSource %s", ex.getMessage());
				log.error(errMsg);
				log.debug("Stack trace :\n", ex);
			}
		});
	}

	public static TenantConnectionProviderBuilder builder() {
		return new TenantConnectionProviderBuilder();
	}

	public static class TenantConnectionProviderBuilder {
		private String defaultTenant;
		private Resource tenantDetailsResource;
		private List<TenantDetails> tenantDetails;

		public TenantConnectionProviderBuilder withDefaultTenant(String defaultTenant) {
			this.defaultTenant = defaultTenant;
			return this;
		}

		public TenantConnectionProviderBuilder withClasspath(String yamlFile) {
			this.tenantDetailsResource = new ClassPathResource(yamlFile);
			return this;
		}

		public TenantConnectionProviderBuilder withFilePath(Path yamlFilePath) {
			this.tenantDetailsResource = new FileSystemResource(yamlFilePath.toString());
			return this;
		}

		public TenantConnectionProvider build() {
			Assert.hasText(defaultTenant, "Default Tenant must be provided (cant be NULL)");
			Assert.notNull(tenantDetailsResource, "Tenant yaml file path must be provided (cant be NULL)");
			tenantDetails = YamlReaderUtil.loadTenantDetails(tenantDetailsResource);
			return new TenantConnectionProvider(tenantDetails, defaultTenant);
		}
	}
}

package com.monetize360.multitenancy.domain;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TenantDetails {

		private String dbUser;
		private String dbPassword;
		private String dbUrl;
		private String tenantId;
}

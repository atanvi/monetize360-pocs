package com.monetize360.multitenancy.mtapp.config;

public class M360TenantContext {

	private static final ThreadLocal<String> CONTEXT = new ThreadLocal<>();

	public static void setTenantId(String tenantId) {
		CONTEXT.set(tenantId);
	}

	public static String getTenantId() {
		return CONTEXT.get();
	}

	public static void clear() {
		CONTEXT.remove();
	}
}

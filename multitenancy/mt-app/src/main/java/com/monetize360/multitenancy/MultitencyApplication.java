package com.monetize360.multitenancy;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.autoconfigure.jdbc.DataSourceTransactionManagerAutoConfiguration;
import org.springframework.boot.autoconfigure.orm.jpa.HibernateJpaAutoConfiguration;

import com.monetize360.multitenancy.dao.ContactRepo;
import com.monetize360.multitenancy.domain.Contact;
import com.monetize360.multitenancy.mtapp.config.M360TenantContext;

@SpringBootApplication(
		exclude = {
				DataSourceAutoConfiguration.class,
				HibernateJpaAutoConfiguration.class,
				DataSourceTransactionManagerAutoConfiguration.class
})
public class MultitencyApplication implements CommandLineRunner{

	public static void main(String[] args) {
		SpringApplication.run(MultitencyApplication.class, args);
	}
	
	@Autowired
	private ContactRepo contactRepo;
	
	public void run(String... args) throws Exception {
		
		M360TenantContext.setTenantId("TENANT_DHI");
		Contact contact1 = new Contact();
		contact1.setName("Dhi User");
		contact1.setEmail("dhi_user@gmail.com");
		contactRepo.save(contact1);
		
		M360TenantContext.setTenantId("TENANT_360");
		Contact contact2 = new Contact();
		contact2.setName("Lakshman");
		contact2.setEmail("lakshman.a@heraizen.com");
		contactRepo.save(contact2);
			
	}

}

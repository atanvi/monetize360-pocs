package com.monetize360.multitenancy.mtapp.config;

import org.hibernate.context.spi.CurrentTenantIdentifierResolver;
import org.springframework.beans.factory.annotation.Value;

public class M360TenantResolver implements CurrentTenantIdentifierResolver {

	@Value("${m60.default_tenant.name}")
	private String DEFAULT_TENANT_ID;

	@Override
	public String resolveCurrentTenantIdentifier() {
		String currentTenantId = M360TenantContext.getTenantId();
		return (currentTenantId != null) ? currentTenantId : DEFAULT_TENANT_ID;
	}

	@Override
	public boolean validateExistingCurrentSessions() {
		return true;
	}

}

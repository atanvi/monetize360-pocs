package com.monetize360.multitenancy.dao;


import org.springframework.data.jpa.repository.JpaRepository;

import com.monetize360.multitenancy.domain.Contact;

public interface ContactRepo extends JpaRepository<Contact,Long> {

}

package com.monetize360.multitenancy.mtapp.config;

import org.hibernate.context.spi.CurrentTenantIdentifierResolver;
import org.hibernate.engine.jdbc.connections.spi.MultiTenantConnectionProvider;
import org.springframework.boot.autoconfigure.orm.jpa.JpaProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import com.monetize360.multitenancy.config.M360DefaultMultiTenantConfiguration;
import com.monetize360.multitenancy.provider.TenantConnectionProvider;

@Configuration
@EnableConfigurationProperties({ JpaProperties.class })
@EnableJpaRepositories(basePackages = { "com.monetize360.multitenancy" }, transactionManagerRef = "txManager")
@EnableTransactionManagement
public class MultiTenantConfiguration extends M360DefaultMultiTenantConfiguration {

	@Bean
	@Override
	public MultiTenantConnectionProvider multiTenantConnectionProvider() {
		return TenantConnectionProvider.builder()
				.withClasspath(this.tenantYamlFileName)
				.withDefaultTenant(this.defaultTenantId).build();
					
	}

	@Bean
	@Override
	public CurrentTenantIdentifierResolver currentTenantIdentifierResolver() {
		return new M360TenantResolver();
	}

}
